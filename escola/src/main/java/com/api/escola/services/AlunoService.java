package com.api.escola.services;

import com.api.escola.models.AlunoModel;
import com.api.escola.repositores.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AlunoService {

    final AlunoRepository alunoRepository;


    public AlunoService(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    public boolean existsBycpf(String cpf) {
        return alunoRepository.existsBycpf(cpf);
    }


    @Transactional
    public AlunoModel save(AlunoModel alunoModel) {
        AlunoModel save = new AlunoModel();
        try {
            save = alunoRepository.save(alunoModel);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());

        }
        return save;
    }

    public Page<AlunoModel> findAll(Pageable pageable) {
        return alunoRepository.findAll(pageable);
    }

}

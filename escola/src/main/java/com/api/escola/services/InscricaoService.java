package com.api.escola.services;

import com.api.escola.models.AlunoModel;
import com.api.escola.models.InscricaoModel;
import com.api.escola.repositores.AlunoRepository;
import com.api.escola.repositores.InscricaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class InscricaoService {

    @Autowired
    private InscricaoRepository inscricaoRepository;


    public InscricaoService(AlunoRepository alunoRepository) {
        this.inscricaoRepository = inscricaoRepository;
    }

//    public boolean existsBycpf(String cpf) {
//        return inscricaoRepository.existsBycpf(cpf);
//    }


    @Transactional
    public InscricaoModel save(InscricaoModel inscricaoModel) {
        InscricaoModel save = new InscricaoModel();
        try{
            save = inscricaoRepository.save(inscricaoModel);

        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());

        }
        return save;
    }


}

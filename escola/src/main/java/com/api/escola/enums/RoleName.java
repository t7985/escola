package com.api.escola.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER;
}

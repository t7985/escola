package com.api.escola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



@SpringBootApplication
@RestController
public class EscolaApplication {

	public static void main(String[] args) {

		SpringApplication.run(EscolaApplication.class, args);
		System.out.println(new BCryptPasswordEncoder().encode("001debora"));
	}

	//https://www.uuidgenerator.net/version1

	@GetMapping("/")

	public String index() {

		return "Bom treinamento!";
	}
}

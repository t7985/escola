package com.api.escola.controllers;


import com.api.escola.dtos.InscricaoDto;
import com.api.escola.models.AlunoModel;
import com.api.escola.models.InscricaoModel;
import com.api.escola.repositores.InscricaoRepository;
import com.api.escola.services.InscricaoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/escola-inscricao", produces = MediaType.APPLICATION_JSON_VALUE)
public class InscricaoController {


    @Autowired
    private InscricaoRepository inscricaoRepository;
    private InscricaoService inscricaoService;

    private InscricaoModel inscricaoModel;

    @PostMapping()
    public ResponseEntity<Object> saveInscricao(@RequestBody @Valid InscricaoDto inscricaoDto) {

        var inscricaoModel = new InscricaoModel();
        BeanUtils.copyProperties(inscricaoDto, inscricaoModel);
        inscricaoModel.setDataInscricao(LocalDateTime.now(ZoneId.of("UTC")));
        return ResponseEntity.status(HttpStatus.CREATED).body(inscricaoService.save(inscricaoModel));

    }


    @GetMapping("/teste")
    public ResponseEntity<List<InscricaoModel>> findAll() {
        List<InscricaoModel> list = inscricaoRepository.findAll();
        return ResponseEntity.ok().body(list);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<InscricaoDto> findById(@PathVariable int id) {

        var inscricaoDto = new InscricaoDto();


        InscricaoModel obj = inscricaoRepository.findById(id).get();

        BeanUtils.copyProperties(obj, inscricaoDto);

        return ResponseEntity.ok().body(inscricaoDto);
    }



    @GetMapping(value = "/matricula/{matricula}")
    public ResponseEntity<List<InscricaoModel>> findBymatricula(@PathVariable String matricula) {
        List<InscricaoModel> list = inscricaoRepository.findBymatricula(matricula);
        return ResponseEntity.ok().body(list);
    }

}

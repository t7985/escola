package com.api.escola.controllers;


import com.api.escola.dtos.AlunoDto;
import com.api.escola.models.AlunoModel;
import com.api.escola.repositores.AlunoRepository;
import com.api.escola.services.AlunoService;
import com.google.gson.Gson;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/escola-aluno", produces = MediaType.APPLICATION_JSON_VALUE)
public class AlunoController {

    final AlunoService alunoService;
    private AlunoModel alunoModel;

    public AlunoController(AlunoService alunoService) {
        this.alunoService = alunoService;
    }

    @PostMapping()
    public ResponseEntity<Object> saveAluno(@RequestBody @Valid AlunoDto alunoDto) throws Exception {

        if(alunoService.existsBycpf(alunoDto.getCpf())){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: Este CPF já esta cadastrado!");
        }

        var alunoModel = new AlunoModel();
        BeanUtils.copyProperties(alunoDto, alunoModel);
        alunoModel.setDataMatricula(LocalDateTime.now(ZoneId.of("UTC")));

        //** Consumindo API Publica Externa

        URL url = new URL("https://viacep.com.br/ws/"+alunoModel.getCep()+"/json/");
        URLConnection connection = url.openConnection();

        InputStream is = connection.getInputStream();

        BufferedReader br = new BufferedReader(new InputStreamReader(is,"UTF-8"));

        String cep = "";
        StringBuilder jsonCep = new StringBuilder();

        while ((cep = br.readLine()) != null)
        {
            jsonCep.append(cep);

        }

        AlunoModel userAux = new Gson().fromJson(jsonCep.toString(), AlunoModel.class);

        alunoModel.setCep(userAux.getCep());
        alunoModel.setLogradouro(userAux.getLogradouro());
        alunoModel.setComplemento(userAux.getComplemento());
        alunoModel.setBairro(userAux.getBairro());
        alunoModel.setLocalidade(userAux.getLocalidade());
        alunoModel.setUf(userAux.getUf());

        //** Consumindo API Publica Externa


        return ResponseEntity.status(HttpStatus.CREATED).body(alunoService.save(alunoModel));
    }

    @GetMapping("/consulta")
    public ResponseEntity<Page<AlunoModel>> getAllParkingSpots(@PageableDefault(page = 0, size = 10, sort = "id", direction = Sort.Direction.ASC) Pageable pageable){
        return ResponseEntity.status(HttpStatus.OK).body(alunoService.findAll(pageable));
    }

}

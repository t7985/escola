package com.api.escola.repositores;

import com.api.escola.models.AlunoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface AlunoRepository extends JpaRepository<AlunoModel, Integer> {

    boolean existsBycpf(String cpf);

}

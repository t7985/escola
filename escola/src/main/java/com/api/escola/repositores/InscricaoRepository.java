package com.api.escola.repositores;

import com.api.escola.models.AlunoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import com.api.escola.models.InscricaoModel;

public interface InscricaoRepository extends JpaRepository <InscricaoModel, Integer>{

    List<InscricaoModel> findBymatricula(String matricula);
}

package com.api.escola.dtos;

import com.api.escola.models.AlunoModel;
import com.api.escola.models.CursoModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InscricaoDto {


    private String matricula;

    public InscricaoDto() {

    }



    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    private CursoModel cursoModel;
    private AlunoModel alunoModel;

    public InscricaoDto(String matricula, CursoModel cursoModel) {

        this.matricula = matricula;
        this.cursoModel = cursoModel;

    }

    public CursoModel getCursoModel() {

        return cursoModel;
    }

    public void setCursoModel(CursoModel cursoModel) {
        this.cursoModel = cursoModel;
    }

    public AlunoModel getAlunoModel() {
        return alunoModel;
    }

    public void setAlunoModel(AlunoModel alunoModel) {
        this.alunoModel = alunoModel;
    }
}

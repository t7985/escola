package com.api.escola.models;

import org.hibernate.annotations.ForeignKey;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tb_Inscricao")
public class InscricaoModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id_Inscricao = 1;

    @Column(nullable = false, length = 10)
    private String matricula;

    @Column(nullable = false)
    private LocalDateTime dataInscricao;

    @ManyToOne()
    @JoinColumn(name = "curso_id")
    @ForeignKey(name = "curso_pk")
    private CursoModel cursoModel;

    @ManyToOne()
    @JoinColumn(name = "aluno_id")
    @org.hibernate.annotations.ForeignKey(name = "aluno_pk")
    private AlunoModel alunoModel;

    public InscricaoModel() {

    }

    public InscricaoModel(String matricula, CursoModel cursoModel, AlunoModel alunoModel) {
        this.matricula = matricula;
        this.cursoModel = cursoModel;
        this.alunoModel = alunoModel;

    }

    public int getId_Inscricao() {
        return id_Inscricao;
    }

    public void setId_Inscricao(int id_Inscricao) {
        this.id_Inscricao = id_Inscricao;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }


    public LocalDateTime getDataInscricao() {
        return dataInscricao;
    }

    public void setDataInscricao(LocalDateTime dataInscricao) {
        this.dataInscricao = dataInscricao;

    }

    public CursoModel getCursoModel() {
        return cursoModel;
    }

    public void setCursoModel(CursoModel cursoModel) {
        this.cursoModel = cursoModel;

    }

    public AlunoModel getAlunoModel() {
        return alunoModel;
    }

    public void setAlunoModel(AlunoModel alunoModel) {
        this.alunoModel = alunoModel;
    }
}

package com.api.escola.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tb_Curso")
public class CursoModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id_curso = 1;

    @Column(nullable = false, length = 70)
    private String nomeCurso;

    @Column(nullable = false, length = 11)

    private String periodo;

    @JsonIgnore
    @OneToMany(mappedBy = "cursoModel")
    private List<InscricaoModel> inscricaoModels = new ArrayList<>();

    public CursoModel() {
    }

    public int getId_curso() {
        return id_curso;
    }

    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public List<InscricaoModel> getInscricaoModels() {
        return inscricaoModels;
    }

    public void setInscricaoModels(List<InscricaoModel> inscricaoModels) {
        this.inscricaoModels = inscricaoModels;
    }
}
